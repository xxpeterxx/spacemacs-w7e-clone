;;; -*- no-byte-compile: t -*-
(define-package "flyspell-correct-helm" "20161031.1134" "correcting words with flyspell via helm interface" '((flyspell-correct "0.4.0") (helm "1.9.0")) :commit "1e19a2b506470e8d741b521da0bd9b66214256f3" :url "https://github.com/d12frosted/flyspell-correct")
