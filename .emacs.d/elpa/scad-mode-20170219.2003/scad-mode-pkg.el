;;; -*- no-byte-compile: t -*-
(define-package "scad-mode" "20170219.2003" "A major mode for editing OpenSCAD code" 'nil :commit "a6e98831985be670148c527747b0e91330d0307b" :url "https://raw.github.com/openscad/openscad/master/contrib/scad-mode.el" :keywords '("languages"))
